import React from 'react'
import '../App.css'

const Footer = () => {
  return (
    <nav className="navbar fixed-bottom navbar-light bg-light footerBar">
      <div className="container footerContainer">
      <h6 className="text-muted">Be your Cavalry</h6>
      <h6 className="text-muted">Rachel Wong Ⓒ 2019 </h6> 
      </div>
    </nav>
  )
}

export default Footer