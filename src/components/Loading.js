import React from 'react'
import Spinner from "../assets/spinner.gif"
import '../App.css'

const Loader = () => {
  return (
    <div className="spinner">
      <img src={Spinner} alt="Loading" width="200" height="200"/>
    </div>
  )
}

export default Loader