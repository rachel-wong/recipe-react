import React from 'react'
import '../App.css'
import {Link} from 'react-router-dom'

// to display one recipe result
const Recipe = ({title, calories, image, ingredients, uri}) => {

  const encodedURI = encodeURIComponent(uri)
  return (
    <div className="recRes card">
      <img className="card-img-top recResImage" src={image} alt={title} />
      <div className="card-body">
        <h3 className="card-title recResTitle">{title}</h3>
        <p className="card-text recResCalories"><strong>Calorie Count:</strong> {calories.toFixed(2)}</p>
        {/* <ul className="recResIngredients">
          {ingredients.map((item, index) => (
            <li key={index}>{item.text}</li>
          ))}
        </ul> */}
        <Link to={`/${encodedURI}`}><button className="btn btn-warning">See more</button></Link>
      </div>
    </div>
  )
}

export default Recipe