import React from 'react';
import './App.css';
// import Spinner from "./assets/spinner.gif"
import Detail from "./pages/Detail"
import Home from "./pages/Home"
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom'

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" exact component={Home}></Route>
          <Route path="/:id" exact component={Detail}></Route>
        </Switch>
      </Router>
    </div>
  )
}

export default App;
