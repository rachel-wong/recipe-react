import React, {useEffect, useState} from 'react';
import '../App.css';
import Recipe from "../components/Recipe"
import Footer from "../components/Footer"
import Loader from "../components/Loading"

const Home = () => {

  const APP_ID= process.env.APP_ID
  const APP_KEY = process.env.APP_KEY

  // create a state to store the recipes returned in the api call
  // the argument in useState is the array of objects returned from data.hits in the json returned from the api call
  const [recipes, setRecipes] = useState([])
  const [searchInput, setSearch] = useState('') // default state is empty string
  const [query, setQuery] = useState("") // create another state to trigger only when submit button is clicked
  // console.log('recipes in the beginning: ', recipes) // recipes has all the json data returned from api call
  const [loading, setLoading] = useState(true)

  const exampleReq = `https://api.edamam.com/search?q=${query}&app_id=${APP_ID}&app_key=${APP_KEY}`

  // Set up the api call outside of eveyrthing else
    const getRecipes = async () => {
    setLoading(true)
    const response = await fetch (exampleReq, {
      "Set-Cookie": "HttpOnly;Secure;SameSite=Strict" // get rid of chrome warning
    })
    const data = await response.json()
    setLoading(false) 
    setRecipes(data.hits) // get all the json data and set it to the recipes variable
  } 

  useEffect(()=> {
    setLoading(false) 
    getRecipes()
  }, [query]) // can pass in the searchInput inside the array argument, but it will trigger every time the input form changes. we only want to make the call when a word has been inputted and the submit button clicked

  // gets the search term from input
  const updateSearch = event => {
    setSearch(event.target.value) // => change the search state variable to whatever that the input form has changed to
  }

  // triggered when submit button is clicked and pass the input into a variable
  const getSearch = event => {
    event.preventDefault() // stops form from automatically submitting
    setQuery(searchInput)
    setSearch("") //reset the input form to nothing once the form is submitted
  }

  return (
    <div>
      <div className="jumbotron headerBG">
        <h1><span role="img" aria-label="image">🍼</span> Edamam <span role="img" aria-label="image">🍴</span> Recipe <span role="img" aria-label="image">🍴</span> Directory <span role="img" aria-label="image">🍡</span></h1>

        <div className="form-group container">
          <form onSubmit={getSearch} type="text" className="search-form">
            <input type="text" className="form-control form-control-lg search-bar" value={searchInput} placeholder="Enter a food name here ..." onChange={updateSearch}/>
            <button type="submit" className="btn btn-light search-button btn-lg active">Search <span role="img" aria-label="image">🍖</span></button>
          </form>
      </div>
      {/* <h1 onClick={()=>setCounter(counter+1)} >{counter}</h1> */}

    </div>

    <div className="container">
        <div className="card-columns recipeAreas">
          {loading ? <Loader/> : recipes.map((recipe, index) => (
          <Recipe key={index} title ={recipe.recipe.label} calories={recipe.recipe.calories} 
          image={recipe.recipe.image} ingredients={recipe.recipe.ingredients} uri={recipe.recipe.uri}/>
        ))}
        </div>
    </div>
      <Footer />
    </div>
  )
}

export default Home;
