import React, {useState, useEffect} from 'react'
import Loader from "../components/Loading"
import {Link} from "react-router-dom"
import Footer from "../components/Footer"
const Detail = ({match}) => {
  const [oneRecipe, setOneRecipe] = useState({})

  // Problem: Parsing array properties from the results object always returns undefined. 
  // Awkward workaround: Init separate states for each array property of the result object.
  // Possible cause: Delay in server responding to api-call leading to capturing the initial undefined result 
  const [healthLabels, setHealthLabels] = useState([])
  const [ingredients, setIngredients] = useState([])
  const [cautions, setCautions] = useState([]) // don't set up default state in here for empty error handling. do it in the render
  const [dietLabels, setDietLabels] = useState([])
  const [digest, setDigest] = useState({})
  const [loading, setLoading] = useState(true)
  const [nutrients, setNutrients] = useState({})

  useEffect(()=> {
    setLoading(true)
    fetchOneRecipe()
  }, [])

  const fetchOneRecipe = async () => {
    
    const APP_ID= process.env.APP_ID
    const APP_KEY = process.env.APP_KEY
    
    // Problem: not sure why cors when queryring with r parameter
    const api = `https://cors-anywhere.herokuapp.com/https://api.edamam.com/search?r=${match.params.id}&app_id=${APP_ID}&app_key=${APP_KEY}`
    
    let data = await fetch(api,  {
      "Set-Cookie": "HttpOnly;Secure;SameSite=Strict" // for chrome error
    })
    let result = await data.json()
    setOneRecipe(result[0])

    // Problem: Can't update state with another state? It has to be direct result from api call.
    if (oneRecipe.healthLabels !== undefined && oneRecipe.healthLabels !== []) {
      setHealthLabels(result[0].healthLabels) // can't be setHealthLabels(oneRecipe.healthLabels)
    } else {
      setHealthLabels(["Totally unhealthy don't do it"])
    }

    if (result[0].ingredientLines !== undefined && result[0].ingredientLines !== []) {
      setIngredients(result[0].ingredientLines) 
    } else {
      setIngredients(["Ingredients, schmedients"])
    }

    if (result[0].cautions !== undefined && result[0].cautions !== []) {
      setCautions(result[0].cautions)
    } // don't set default values here either. won't show up. do the error handling in the render

    if (result[0].dietLabels !== undefined && result[0].dietLabels !== []) {
      setDietLabels(result[0].dietLabels)
    } else {
      setDietLabels(["Not special 🍭"])
    }

    if (oneRecipe.digest !== undefined || oneRecipe.digest !== null) {
      setDigest(result[0].digest)
    }
    
    if (oneRecipe.totalNutrients !== undefined || oneRecipe.totalNutrients !== null) {
      setNutrients(result[0].totalNutrients)
    }
    
    // if (result[0].totalNutrients !== undefined || result[0].totalNutrients !== []) {
    //   setNutrients(result[0].totalNutrients)
    // } 
    setLoading(false) // turn off loading when all data is available
  }
  console.log(oneRecipe)
  // console.log(cautions)
  return (
    <div>
      {loading ? <Loader/> : 
      <div className="mb-5">
        <div className="jumbotron recipeBG">

          <div className="container">
            {/* breadcrumb back */}
            <nav aria-label="breadcrumb" className="border-0 bg-transparent">
            <ol className="breadcrumb bg-transparent">
              <Link to="/"><li className="breadcrumb-item active bg-transparent text-white" aria-current="page"><span role="img" aria-label="Go back home">👈</span> Back</li></Link>
            </ol>
          </nav>

          <div className="recipeHeading">
            <h1 className="display-4">{oneRecipe.label}</h1>
            <div className="cautionLabels">
              {cautions.length > 0 ? cautions.map((item, idx) => (
                <button type="button" key={idx} className="btn btn-outline-light ml-2">{item}</button>
              )) : <button type="button" className="btn btn-outline-light ml-2">SAFE <span role="img" aria-label="okay to eat">👌</span></button>} 
            </div>
          </div>
            <hr className="my-4" />
          </div>  
          </div>
        
        {/* main content area */}
        <div className="container mb-5">
        <div className="row mb-5">
            {/* big left column */}
            <div className="col-sm-8">
            <div className="mb-4">
            {dietLabels.length > 0 ? dietLabels.map((item, idx) => (
              <button type="button" key={idx} className="btn btn-light btn-sm ml-2">{item}</button> 
            )): 
              <button type="button" className="btn btn-light btn-sm ml-2"><span role="img" aria-label="no diet label applies">✌️</span>Nothing special</button> }
            </div>
              <h4 className="mb-3"><strong>Ingredients</strong></h4>
              <table className="table table-hover table-borderless">
                <tbody>
                    {ingredients.map((item, idx) => (
                      <tr key={idx}>
                          <td>{item} </td>
                          <td><button type="button" className="btn btn-secondary btn-sm">Go shopping</button></td> 
                          </tr>
                        ))}
              </tbody>
              </table>
            <a href={oneRecipe.url}><button className="btn btn-warning">Original Recipe</button></a> 
            </div> 
            {/* small right column */}
            <div className="col-sm-4">
              <img src={oneRecipe.image} alt={oneRecipe.title} />
              <p className="mt-4"><strong>Calories:</strong> {oneRecipe.calories.toFixed(2)}</p>
              <p><strong>Total Serving weight: </strong>{parseInt(oneRecipe.totalWeight)}</p>
              <br />
              <h5 className="text-muted"><strong>Scorecard</strong></h5>
              {digest.map((item, idx) => (
                <div key={idx} className="btn-group ml-2 mb-2" role="group" aria-label="Basic example">
                  <button type="button" className="btn btn-info btn-sm">{item.label}</button>
                  <button type="button" className="btn btn-light btn-sm">{item.total.toFixed(2)}</button>
                </div>
              ))}
            </div>
            {/* end row */}
          </div>
          {/* end container */}
        </div>
      </div>
      }
      <Footer></Footer>
    </div>
  )
}

export default Detail