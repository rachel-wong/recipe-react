## Recipe React app

> A simple API call returning results to revise React Hooks

![walkthru_progress](/src/assets/recipe_walkthru2.gif)

### :8ball: The Approach

The API of choice is [Edamam](https://developer.edamam.com), which has a range of search parameters and detailed documentation to really dig deep into the available data. 1000 calls/day.

- basic search using `q` parameter of the Edamam api call
- display results
- learn `react-router-dom` and hooks

### :beetle: Bugs

* Loader always appear upon page load. This is technically correct in that there is no data to display. But ideally, theres should be no data on pageload. The loader should only appear when there is no return after an api call has been triggered.
* In the `Detail` component, there is a lag between server responding and having result captured and mapped onto DOM. I haven't yet found the (appropriate) way to accomodate for this other than to set up separate `useState` for properties and error-handle for `null` or `undefined` values, before assigning anything to a global constant for DOM-displaying. Is the solution to set a `setTimeOut`, if so isn't this a bit arbitrary and brittle? 🙊
* Strange that the EDAMAM api call using the `r` parameter to return a single recipe result is blocked by CORS. Circumvented using `cors-anywhere` again, but obviously not ideal for production purposes.
* It seems a single page load of the `Detail` view triggers 7 api calls on the network. *Unsolved* 🙉👽💩

### :construction: Extensible features

* ~~On click on each recipe result to open dedicated page on recipes~~
* enable a favourites functionality perhaps?
* ~~add a data spinner~~
* A back button using `history`
* Add tooltips or separate pages describing what the caution labels, health labels, diet labels actually mean

### :gift: Helpful resources

- [This article](https://blog.logrocket.com/react-hooks-cheat-sheet-unlock-solutions-to-common-problems-af4caf699e70/) covered a range of common things achievable with hooks and provided some clues as to how to configure a spinning loader.
- [Lottie Animations](https://lottiefiles.com) for the spinning loader
- [A riff off from inspiration](https://www.youtube.com/watch?v=Law7wfdg_ls&t=1206s)

### :cyclone: What I am focusing on learning here - Hooks! useEffect() & useState()

**useEffect()**
```javascript
useEffect(() => {
  // change the state in here
  // trigger the api call here
  useStateMethod()
}, [])
```

- useEffect(callback) is the trigger to run something that changes the state
- the argument at the tail end of useEffect dictates when useEffect runs.
- [] = useEffect runs once when the application mounts (ideal when making calls to external apis)
- [variable] = useEffect runs when the variable inside changes

**useState()**

```javascript
const [theState, theMethodThatTriggersUseEffect] = useState(defaultVal)
```

- useState initialises a variable and a method to input data into variable when useEffect is triggered
- useState(value) initialises the state variable with a default value
- theMethod initialises in useState can be attached to anywhere in the DOM (i.e. when the button clicks, theMethod runs useEffect)

**React Router Dom**

- can't use the `<Link>` to escape to outside of the application it seems
- `{ match }` and `match.params.uri` is the magic glue to link up between the index page and single result pages (`Detail` component)